#include "MotorController.h"



DigitalOut SLEEP(p12);


MotorController::MotorController(PinName _en, PinName m0, PinName m1, PinName m2, PinName _stepPin, PinName dir, int id)
{
    direction = PUSH;
    numOfRotations = 0;   
    speed = 2000;
    steps = 4800;
    step = 1;
    this->id = id;
    motor = new DRV8825(_en, m0, m1, m2, _stepPin, dir);
}

MotorController::~MotorController()
{
    motor->disable();   
}

bool MotorController::init()
{
    motor->enable();
    SLEEP = 1;
    return true;
}

int MotorController::pumpWater()
{
    if (this->numOfRotations == MAX_ROTATIONS)
    {
        this->numOfRotations = 0;
        return 0;
    }
    
    if (direction == PULL)
    {
        // Motor B is reversed PUSH/PULL, so change dir temporarily
        // And restore it afterwards
        if (id == 1)
            direction = PUSH;
        
        /*for (int j = 0; j < MAX_ROTATIONS; j++)
        {
            for ( int i = 0; i < steps; i++)
            {
                motor->settings(step, direction, speed);
                wait_us(200);
            }
        }*/
        
        for (int i = 0; i < steps; i++)
        {
            motor->settings(step, direction, speed);
            wait_us(200);   
        }
        
        if (id == 1)
            direction = PULL;
        
        this->numOfRotations++;
        return 1;
    }
    
    if (direction == PUSH)
    {
        // Motor B is reversed PUSH/PULL, so change dir temporarily
        // And restore it afterwards
        if (id == 1)
            direction = PULL;
            
        for ( int i = 0; i < steps; i++)
        {
            motor->settings(step, direction, speed);
            wait_us(200);
        }
        
        // Restore dir
        if (id == 1)
            direction = PUSH;
        
        
        this->numOfRotations++;
        SLEEP = 1;
        return 2;
    }
    
    return -1;
}

void MotorController::changeDirection()
{
    if (direction == PULL)
    {
        direction = PUSH;
    }
    else if (direction == PUSH)
    {
        direction = PULL;
    }
    return;
}


bool MotorController::shutDown()
{
    SLEEP = 1;
    motor->disable();
    direction = UNKNOWN;
    return true;
}

void MotorController::calibrationMode(int d)
{
    // Perform one "step" with direction d.
    for (int i = 0; i < steps; i++)
    {
        motor->settings(step, d, speed);
    }   
}
    
    
    