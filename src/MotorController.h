#ifndef MOTORCONTROLLER_H
#define MOTORCONTROLLER_H

#include "mbed.h"
#include "math.h"
#include "DRV8825.h"

#define MAX_SPEED 8000
#define MICROSTEPS_PER_STEP 14
#define MAX_ROTATIONS 16


class MotorController 
{

private:
    enum Direction {
        PULL,
        PUSH,
        UNKNOWN
    };
    
    
    Direction direction;    
    unsigned int numOfRotations; //yet to be defined    
    unsigned int speed;
    unsigned int steps;
    unsigned int step;
    int id;
    DRV8825* motor;
    
public: 
    MotorController(PinName _en, PinName m0, PinName m1, PinName m2, PinName _stepPin, PinName dir, int id);
    ~MotorController();
    
    bool init();
    int pumpWater();
    void changeDirection();
    bool shutDown();
    
    void calibrationMode(int d);
};

#endif