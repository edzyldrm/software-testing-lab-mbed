#include "mbed.h"
#include "TextLCD.h"
#include "math.h"
#include "MotorController.h"
#include "AlarmEnum.h"

#define MIN_TEMPERATURE 20.0f
#define MAX_TEMPERATURE 30.0f

#define MIN_SALINITY 10.0f
#define MAX_SALINITY 15.0f

#define MAX_PUMP_COUNTER 200

// 
int pump_counter = 0;

// Timers for alarm functionality
Ticker timer;

// LCD screen
TextLCD lcd(p22, p21, p23, p24, p25, p26);  

// Led switches
DigitalOut onled1 (LED1);

// physical switches 
DigitalIn switch1(p18);
DigitalIn switch2(p30);
DigitalIn switch3(p8);
DigitalIn switch4(p28);

// Sensors Input
AnalogIn temperatureSensor (p19);
AnalogIn salinitySensor    (p20);

// Circular buffer for temperature
float temperature;
int alarmCounter = 0;
float temp_buffer[5] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
unsigned int temp_ind = 0;
unsigned int temp_count = 0;

// Circular buffer for salinity
float salinity;
float sal_buffer[5] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
unsigned int sal_ind = 0;
unsigned int sal_count = 0;

// Heater functionality
bool isHeater = false;
DigitalOut HTR(p11);
DigitalOut redLed(p9);
DigitalOut beep(p13);

DigitalOut caliLed1(p27);

// Motors
MotorController* m0;
MotorController* m1;




float measureSalinity(float sensorValue)
{
    float vout = sensorValue * 3.3f;
    
    /*
    lcd.printf("vout = %f", vout);
    wait(3.0);
    lcd.cls();
    */
    //sal_buffer[sal_ind] = (mult * vout) - minus;
    const float intercept = 11.243f;
    const float voltage = 35.94f;
    const float voltageS = 2.633f;
    const float voltageR = -32.648f;
    
    float vpower = pow(vout, 2);
    float vroot = sqrt(vout);
    
    sal_buffer[sal_ind] = intercept + (voltage * vout) + (voltageS * vpower) + (voltageR * vroot);
    sal_ind = (sal_ind+1) % 5;
    sal_count++;
    sal_count = ((sal_count > 5) ? 5 : sal_count);
    
    return (float) (sal_buffer[0] + sal_buffer[1] + sal_buffer[2] + sal_buffer[3] + sal_buffer[4] ) / (float) sal_count;    
}

float measureTemperature(float sensorValue)
{
    const float R1 = 15000.0f;
    const float e7 = 1e-7;
    const float k0 = 0.00102119f;
    const float k1 = 0.000222468f;
    const float k2 = 1.33342 * e7;
    
    float Rt = ((-1.0f) * R1 * sensorValue) / (sensorValue - 1.0f);
    float divider = k0 + (k1 * (log(Rt))) + (k2 * (pow(log(Rt), 3)));
    float kelvin = 1.0f / divider;
    
    // Circular buffer
    // Take into account only past five values
    temp_buffer[temp_ind] = (float)(kelvin - 273.15f);
    temp_ind = (temp_ind+1)%5;
    temp_count++;
    temp_count = ((temp_count > 5) ? 5 : temp_count);
    
    return (float)(temp_buffer[0]+temp_buffer[1]+temp_buffer[2]+temp_buffer[3]+temp_buffer[4]) / (float)temp_count;
}

void displaySensorValues(float salinity, float temperature)
{
        std::string s = getSalStatus();
        std::string t = getTempStatus();
        
        printf("S=%.2f %s\r\n", salinity, s.c_str());
        printf("T=%.2f %s\r\n", temperature, t.c_str());
        printf("Pump_counter: %d\r\n", pump_counter);
        printf("Alarm counter: %d\r\n", alarmCounter);
        printf("------------------------------------------------------------\n");
         
        lcd.cls();
        lcd.locate(0, 0);
        lcd.printf("S=%.2f %s", salinity, s.c_str());
        lcd.locate(0, 1);
        lcd.printf("T=%.2f %s", temperature, t.c_str());
}

void alarm()
{
    if (tempStatus == UNKNOWN && prevTempStatus == UNKNOWN)
    {
        ;    
    }else
    {
        prevTempStatus = tempStatus;   
    }
    
    if (salStatus == S_UNKNOWN && prevSalStatus == S_UNKNOWN)
    {
        ;
    }
    else
    {
        prevSalStatus = salStatus;
    }
    
    if (temperature < -5)
        tempStatus = UNKNOWN;
    else if (temperature >= -5 && temperature <= 10)
        tempStatus = VERY_COLD;
    else if (temperature > 10 && temperature < 20)
        tempStatus = COLD;
    else if (temperature >= 20 && temperature <= 30)
        tempStatus = NORMAL;
    else if (temperature > 30 && temperature <= 40)
        tempStatus = WARM;
    else if (temperature > 40 && temperature <= 50)
        tempStatus = HOT;
    else if (temperature > 50 && temperature <= 60)
        tempStatus = VERY_HOT;
    else if (temperature > 60 && temperature <= 100)
        tempStatus = DANGEROUS;
    else
        tempStatus = UNEXPECTED;
        
    if (salinity < 0)
        salStatus = S_UNEXPECTED;
    else if (salinity >= 0 && salinity <= 5)
        salStatus = VERY_LOW;
    else if (salinity > 5 && salinity < 10)
        salStatus = LOW;
    else if (salinity >= 10 && salinity <= 15)
        salStatus = S_NORMAL;
    else if (salinity > 15 && salinity <= 25)
        salStatus = HIGH;
    else if (salinity > 25 && salinity <= 35)
        salStatus = VERY_HIGH;
    else if (salinity > 35 && salinity <= 50)
        salStatus = S_DANGEROUS;
    else
        salStatus = S_UNEXPECTED; 
    
    if (tempStatus == VERY_HOT)
    {
        alarmCounter++;
        beep = 1;
        wait (0.1);
        beep = 0;
    }
    else if (tempStatus == DANGEROUS)
    {
        beep = 1;
        alarmCounter++;
        wait (0.1);
        beep = 0;
    }        
    else if (tempStatus == UNEXPECTED)
    {   
        beep = 1; 
        alarmCounter++;
        wait (0.1);
        beep = 0;
    }
    
    if (salStatus == S_DANGEROUS)
    {
        beep = 1;
        wait(0.05);
        beep = 0;
        wait(0.05);
        beep = 1;
        wait(0.05);
        beep = 0;
    }
    
    /*
    lcd.cls();
    lcd.locate(0, 0);
    if ( tempStatus == UNKNOWN)
    {
        beep = 1;
        lcd.printf("T: UNKNOWN");
        wait(1.0);
        beep = 0;    
    }
    else if (tempStatus == VERY_COLD)
    {
        beep = 1;
        lcd.printf("T: VERY COLD");
        wait(1.0);
        beep = 0;   
    }
    else if (tempStatus == COLD)
    {
        ;
    }
    else if (tempStatus == NORMAL)
    {
        ;   
    }
    else if (tempStatus == WARM)
    {
        ;
    }
    else if (tempStatus == HOT)
    {
        beep = 1;
        lcd.printf("T: HOT");
        wait(1.0);
        beep = 0;
    }
    else if (tempStatus == VERY_HOT)
    {
        alarmCounter++;
        beep = 1;
        lcd.printf("T: VERY HOT");
        wait(1.0);
        beep = 0;
    }
    else if (tempStatus == DANGEROUS)
    {
        alarmCounter++;
        beep = 1;
        lcd.printf("T: DANGEROUS");
        wait(1.0);
        beep = 0;
    }
    else if (tempStatus == UNEXPECTED)
    {
        alarmCounter++;
        beep = 1;
        lcd.printf("T: UNEXPECTED");
        wait(1.0);
        beep = 0;
    }
    
    
    lcd.locate(0,1);
    if (salStatus == S_UNKNOWN)
    {
        beep = 1;
        lcd.printf("S: UNKNOWN");
        wait(1.0);
        beep = 0;    
    }
    else if (salStatus == VERY_LOW)
    {
        beep = 1;
        lcd.printf("S: VERY_LOW");
        wait(1.0);
        beep = 0;     
    }
    else if (salStatus == LOW)
    {
        ;
    }
    else if (salStatus == S_NORMAL)
    {
        ;
    }
    else if (salStatus == HIGH)
    {
        ;
    }
    else if (salStatus == VERY_HIGH)
    {
        beep = 1;
        lcd.printf("S: VERY HIGH");
        wait(1.0);
        beep = 0;    
    }
    else if (salStatus == S_DANGEROUS)
    {
        beep = 1;
        lcd.printf("S: DANGEROUS");
        wait(1.0);
        beep = 0;
    }
    else if (salStatus == S_UNEXPECTED)
    {
        beep = 1;
        lcd.printf("S: UNEXPECTED");
        wait(1.0);
        beep = 0;        
    }
    */
    
    return;
}

void regulateTemperature(float temperature)
{      
    if (temperature < (MAX_TEMPERATURE-5.0f))
    {
        // heater on
        if (isHeater)
            return;
        isHeater = true;
        // turn on heater
        lcd.cls();
        lcd.locate(0, 0);
        lcd.printf("Turn on heater\n in 2sec");
        beep = 1;
        wait(2.0);
        beep = 0;
        HTR = 1;
        redLed = 1;
        lcd.cls();
        return;
    }
    
    if (isHeater)
    {
        isHeater = false;
        // turn off heater
        lcd.cls();
        lcd.locate(0, 0);
        lcd.printf("Heater is off\nin 1sec");
        beep = 1;
        wait(1.0);
        beep = 0;
        HTR = 0;
        redLed = 0;
        lcd.cls();
    }   
    return;
}

bool activatePump(MotorController* m)
{
    int i = -2;
    i = m->pumpWater();
    if ( i == 0 )
    {
        m->changeDirection();
        lcd.cls();
        lcd.locate(0, 0);
        lcd.printf("Change VALVE\n direction");
        while( switch3 == true)
        {
            wait_us(100);
        }
        lcd.cls();
        return true;
    }
    else if ( i == 1 )
    {
        return true;   
    }
    else if ( i == 2 )
    {
        pump_counter++;
        return true;   
    }
    
    lcd.cls();
    lcd.locate(0, 0);
    lcd.printf("UNKNOWN\n CODE");
    wait(30);
    lcd.cls();
    return false;
}


bool regulateSalinity(float salinity)
{
    bool result = true;
    if (salinity < (MIN_SALINITY+2.5f))
    {
        // Pump salty
        result = activatePump(m1);
    }
    
    if (salinity > (MAX_SALINITY-2.5f))
    {
        // Pump fresh
        result = activatePump(m0); 
    }
    return result;
}


void calibrateMotors()
{
    lcd.cls();
    caliLed1 = 1;
    
    lcd.locate(0, 0);
    lcd.printf("Calibration Mode:");
    lcd.locate(0, 1);
    lcd.printf("Clean Water");
    // Calibrate pure water syringe loop
    while (switch2 == false)
    {
        // PULL
        if (switch3 == 0)
        {
            m0->calibrationMode(0);
        }      
            
        // PUSH
        if (switch4 == 0)
        {
            m0->calibrationMode(1);
        }        
    }
    
    lcd.cls();
    lcd.locate(0, 0);
    lcd.printf("Calibration Mode:");
    lcd.locate(0, 1);
    lcd.printf("Salty Water");
    while (switch2 == true)
    {
        // PULL 
        if (switch3 == 0)
        {
            m1->calibrationMode(1);
        }
            
        // PUSH
        if (switch4 == 0)
        {
            m1->calibrationMode(0);
        }
            
    }
    lcd.cls();
    caliLed1 = 0;
    return;
}

void exit()
{
    beep = 0;
    HTR = 0;
    onled1 = 0;
    redLed = 0;
    m0->shutDown();
    m1->shutDown(); 
    timer.detach(); 
}

int main ()
{
    beep = 0;
    onled1 = 0;;
    redLed = 0;
    m0 = new MotorController(p5, p16, p15, p14, p6, p7, 0); // last field = ID
    m0->init();
    m1 = new MotorController(p29, p16, p15, p14, p17, p10, 1); // last field = ID
    m1->init();
    
    // Motor Calibrations
    calibrateMotors();
    
    // timer for temperature alarm
    timer.attach(&alarm, 5);
    
    bool running = true;
    while (running)
    {
        while (!switch1)
        {
            float inputS = salinitySensor.read();
            salinity = measureSalinity(inputS);
        
            float inputT = temperatureSensor.read();
            temperature = measureTemperature(inputT);
        
        
            printf("------------------------------------------------------------\r\n");
            printf("Sal voltage = %.2f\r\n", inputS * 3.3f);
            printf("Temp voltage = %.2f\r\n", inputT * 3.3f);
            displaySensorValues(salinity, temperature);
        
            /*lcd.locate(0,0);
            lcd.printf("%.2f", 16.3f * 3.3f * inputV);
            lcd.locate(7, 0);
            lcd.printf("%.2f", 3.3f * inputV * 2.0f * 17.5f);
        
            lcd.locate(0,1);
            lcd.printf("volt=%.2f\n", inputV * 3.3f);
            wait (0.8);
            lcd.cls();*/
        
            if (prevTempStatus != tempStatus)
            {
                alarmCounter = 0;
            }
        
        
            if (alarmCounter == 5)
            {
                lcd.cls();
                lcd.locate(0, 0);
                lcd.printf("Shutting down\n DANGER");
                beep = 1;
                wait(3.0);
                running = false;
                break;
            }    
        
            if (tempStatus == VERY_COLD ||
                tempStatus == COLD ||
                isHeater == true)
            {
                regulateTemperature(temperature);
            }    
        
        
            // Regulate salinity in range of 15-25PPT
            if (salStatus == VERY_LOW ||
                salStatus == LOW ||
                salStatus == HIGH ||
                salStatus == VERY_HIGH)
            {
                if (regulateSalinity(salinity) == false)
                {
                    lcd.cls();
                    lcd.locate(0,0);
                    lcd.printf("UNKNOWN\nERROR");
                    wait(3.0f);
                    running = false;
                    break;
                }
            }
        
            // Prevent overflowing of the system
            if (pump_counter == MAX_PUMP_COUNTER)
            {
                lcd.cls();
                lcd.locate(0, 0);
                beep = 1;
                lcd.printf("SYSTEM\nOVERFLOW!!!");
                wait(2.5);
                beep = 0;
                running = false;
                break;
            }
        
            // Print values to LCD screen
            //displaySensorValues(salinity, temperature);
            wait(0.8);
        }
        
        if ( switch1 )
        {
            lcd.cls();
            lcd.printf("System Paused");
            wait(0.8);
        }   
    }
    
    lcd.cls();
    lcd.locate(0,0);
    lcd.printf("Operation End\nShutting down");
    wait(0.2);
    exit();
    
    return 0;
}