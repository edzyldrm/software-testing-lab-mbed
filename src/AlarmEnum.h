#pragma once
#include <string>

enum TEMP_STATUS {
    UNKNOWN = 0,
    VERY_COLD,
    COLD,
    NORMAL,
    WARM,
    HOT,
    VERY_HOT,
    DANGEROUS,
    UNEXPECTED
} tempStatus;

enum TEMP_STATUS prevTempStatus = UNKNOWN;



enum SAL_STATUS {
    S_UNKNOWN = 0,
    VERY_LOW,
    LOW,
    S_NORMAL,
    HIGH,
    VERY_HIGH,
    S_DANGEROUS,
    S_UNEXPECTED
} salStatus;

enum SAL_STATUS prevSalStatus = S_UNKNOWN;

std::string getTempStatus()
{
    switch(tempStatus)
    {
        case UNKNOWN:
            return "UNKNOWN";
        case VERY_COLD:
            return "VERY COLD";
        case COLD:
            return "COLD";
        case NORMAL:
            return "NORMAL";
        case WARM:
            return "WARM";
        case HOT:
            return "HOT";
        case VERY_HOT:
            return "VERY HOT";
        case DANGEROUS:
            return "DANGEROUS";
        case UNEXPECTED:
            return "UNEXPECTED";
        default:
            return "Default";
    }   
}

std::string getSalStatus()
{
    switch(salStatus)
    {
        case S_UNKNOWN:
            return "UNKNOWN";
        case VERY_LOW:
            return "VERY LOW";
        case LOW:
            return "LOW";
        case NORMAL:
            return "NORMAL";
        case HIGH:
            return "HIGH";
        case VERY_HIGH:
            return "VERY HIGH";
        case S_DANGEROUS:
            return "DANGEROUS";
        case S_UNEXPECTED:
            return "UNEXPECTED";
        default:
            return "Default"; 
    }
}